import logo from "./logo.svg";
import "./App.css";
import { useParams } from "react-router-dom";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/home";
import HomeWithName from "./pages/homewithName";

function App() {
  const params = useParams();
  const link = [
    {
      path: "/:params",
      component: <HomeWithName />,
    },
    {
      path: "/",
      component: <Home />,
    },
  ];
  return (
    <div className="App">
      <header>
        <Routes>
          {link.map((data, i) => (
            <Route key={i} path={data.path} element={data.component}></Route>
          ))}
        </Routes>
      </header>
    </div>
  );
}

export default App;
