import "../App.css";
import { Routes, Route } from "react-router-dom";
import image from "./assets/Frame_8_7.png";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";

function Home() {
  return (
    <div className="container-md bg-image">
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: "100vh" }}
      >
        <Grid
          container
          direction="row"
          justifyContent="flex-start"
          alignItems="center"
          className="grid-text1"
        >
          <h1 className="title">Dear</h1>
        </Grid>
        <Grid
          container
          direction="row"
          justifyContent="flex-start"
          alignItems="center"
          className="grid-text2"
        >
          <h1 className="title">Masukkan Parameter</h1>
        </Grid>
        <Grid item xs={3} className="grid-button">
          <a href={"https://rafliaran12.wixsite.com/the-wedding-of-a-k"}>
            <button
              style={{
                textTransform: "none",
                fontFamily: "Playfair Display",
                backgroundColor: "#DE136B",
                borderRadius: "100px",
                color: "white",
                paddingBottom: "0.7rem",
                height: "60px",
                fontWeight: "600",
              }}
              className="button"
            >
              Open Invitation
            </button>
          </a>
        </Grid>
      </Grid>
    </div>
  );
}

export default Home;
